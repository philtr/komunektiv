require 'dm-core'
require 'dm-migrations'

# initialize DataMapper
DataMapper.setup(:default, ENV['DATABASE_URL'] || 'sqlite3://db/komunektiv.db')

class Blip
  include DataMapper::Resource
  
  property :id,           Serial
  property :title,        Text
  property :description,  Text
  property :date,         DateTime
  property :source,       Text
  property :link,         Text
  property :source_url,    Text
  
  def self.summary(size = nil)
    if size.nil?
      self.all(:order => [:date.desc])
    else
      self.all(:order => [:date.desc], :limit => size)
    end
  end
  
  def self.potatoes()
  end
  
  def shortdate
    @date.strftime("%h %d")
  end
end

DataMapper.auto_upgrade!
DataMapper.finalize