require 'rubygems'
require 'sinatra'

require 'data/models'

get '/' do
  @blips = Blip.summary
  erb :index
end
